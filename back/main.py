import webview

if __name__ == "__main__":
    webview.create_window(
        "Weather App",
        "dist/index-min.html",
        width=900,
        height=1000,
    )
    webview.start()
